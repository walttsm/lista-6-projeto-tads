package br.unifil.edu;

public interface Lista<T> extends Colecao<T> {
    /**
     * Acessa o elemento localizado nesta posição da lista.
     * @param indice Índice do elemento a ser acessado.
     * @return O elemento acessado.
     */
    T acessar(int indice);

    /**
     * Adiciona um elemento à lista. Altera a lista.
     * @param indice Índice onde o elemento será adicionado.
     * @param obj Elemento a ser adicionado.
     */
    void inserir(int indice, T obj);

    /**
     * Remove um elemento da lista. Altera a lista.
     * @param indice Índice onde de onde o elemento será removido.
     * @return O elemento removido.
     */
    T remover(int indice);

    /**
     * Busca um elemento na lista. Não altera a lista.
     * @param obj Elemento a ser buscado.
     * @return Índice onde o elemento está armazenado na lista.
     */
    int pesquisar(T obj);

    /**
     * Checa se o elemento existe na lista. Não altera a lista.
     * @param obj Elemento de checagem.
     * @return TRUE se o elemento existir na coleção, senão retorna FALSE.
     */
    boolean existe(T obj);
}
