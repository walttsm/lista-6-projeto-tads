package br.unifil.edu;

import java.util.function.Function;

public interface Fila<T> extends Colecao<T> {
    /**
     * Adiciona um elemento ao fim da fila. Altera a fila.
     * @param obj Elemento adicionado à fila.
     */
    void enfileirar(T obj);

    /**
     * Remove o elemento na primeira posição da fila. Altera a fila.
     * @return Elemento removido.
     */
    T desenfileirar();

    /**
     * Verifica o elemento na primeira posição da fila. Não altera a fila.
     * @return O primeiro elemento na fila.
     */
    T olharPrimeiro();
}
