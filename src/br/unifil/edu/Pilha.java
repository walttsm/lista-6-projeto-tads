package br.unifil.edu;

public interface Pilha<T> extends Colecao<T> {
    /**
     * Adiciona um elemento ao topo da pilha.
     * @param obj Elemento a ser colocado na pilha.
     */
    void empilhar(T obj);

    /**
     * Desempilha o elemento no topo da pilha.
     * @return O elemento desempilhado.
     */
    T desempilhar();

    /**
     * Busca qual elemento está no topo da pilha.
     * Não altera a pilha.
     * @return Retorna o elemento, sem removê-lo da pilha.
     */
    T olharTopo();
}
