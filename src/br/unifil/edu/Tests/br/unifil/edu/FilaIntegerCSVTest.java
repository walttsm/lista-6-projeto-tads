package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FilaIntegerCSVTest {
    Fila<Integer> filaVazia;
    Fila<Integer> filaPreenchida;

    @Before
    public void setup() {
        filaVazia = new FilaIntegerCSV();
        filaPreenchida = new FilaIntegerCSV("2,1,5,6");
    }

    @Test
    public void testToString() {
        assertEquals("", filaVazia.toString());
        assertEquals("2,1,5,6", filaPreenchida.toString());
    }

    @Test
    public void enfileirar() {
        filaVazia.enfileirar(2);
        filaVazia.enfileirar(5);
        assertEquals("5,2", filaVazia.toString());
        filaPreenchida.enfileirar(7);
        assertEquals("7,2,1,5,6", filaPreenchida.toString());
    }

    @Test
    public void desenfileirar() {
        assertEquals((Integer ) 6, filaPreenchida.desenfileirar());
        filaVazia.enfileirar(2);
        filaVazia.enfileirar(5);
        assertEquals((Integer) 2, filaVazia.desenfileirar());

    }

    @Test
    public void olharPrimeiro() {
        filaVazia.enfileirar(2);
        filaVazia.enfileirar(5);
        assertEquals((Integer) 2, filaVazia.olharPrimeiro());
        filaVazia.desenfileirar();
        assertEquals((Integer) 5, filaVazia.olharPrimeiro());
        assertEquals((Integer) 6, filaPreenchida.olharPrimeiro());
        filaPreenchida.desenfileirar();//-> tira 6, primeiro é 5
        filaPreenchida.desenfileirar();//-> tira 5, primeiro é 1
        assertEquals((Integer) 1, filaPreenchida.olharPrimeiro());
    }

    @Test
    public void qtdeElems() {
        assertEquals(0, filaVazia.qtdeElems());
        assertEquals(4, filaPreenchida.qtdeElems());
        filaVazia.enfileirar(5);
        assertEquals(1, filaVazia.qtdeElems());
    }

    @Test
    public void isVazia() {
        assertTrue(filaVazia.isVazia());
        assertFalse(filaPreenchida.isVazia());
    }
}