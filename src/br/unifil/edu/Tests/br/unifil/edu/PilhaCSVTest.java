package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import java.lang.Double;
import java.util.function.Function;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public class PilhaCSVTest {
    private PilhaCSV<Double> pilhaDouble;

    @Before
    public void setUp() throws Exception {
        Function<String, Double> doubleFromString = Double::valueOf;
        Function<Double, String> stringifyDouble = String::valueOf;
        pilhaDouble = new PilhaCSV<>(doubleFromString, stringifyDouble, "16.8,1.005");
    }

    @Test
    public void empilhar() {
        pilhaDouble.empilhar(3.0);
        assertEquals("16.8,1.005,3.0", pilhaDouble.toString());
        pilhaDouble.empilhar(2.5);
        assertEquals("16.8,1.005,3.0,2.5", pilhaDouble.toString());
    }

    @Test
    public void desempilhar() {
        assertEquals((Double) 1.005, pilhaDouble.desempilhar());
        assertEquals((Double) 16.8, pilhaDouble.desempilhar());
    }

    @Test
    public void olharTopo() {
        assertEquals((Double) 1.005, pilhaDouble.olharTopo());
        pilhaDouble.desempilhar();
        assertEquals((Double) 16.8, pilhaDouble.olharTopo());

    }

    @Test
    public void qtdeElems() {
        assertEquals(2, pilhaDouble.qtdeElems());
        pilhaDouble.empilhar(6.0);
        assertEquals(3, pilhaDouble.qtdeElems());
    }

    @Test
    public void isVazia() {
        pilhaDouble.desempilhar();
        pilhaDouble.desempilhar();
        assertTrue(pilhaDouble.isVazia());
        pilhaDouble.empilhar(6.0);
        assertFalse(pilhaDouble.isVazia());
    }
}