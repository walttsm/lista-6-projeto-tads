package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.*;

public class ListaCSVTest {
    private ListaCSV<Double> listaDouble;
    private ListaCSV<Double> listaDoubleVazia;

    @Before
    public void setUp() throws Exception {
        Function<String, Double> doubleFromString = Double::valueOf;
        Function<Double, String> stringifyDouble = String::valueOf;
        listaDouble = new ListaCSV<>(doubleFromString, stringifyDouble, "1.0,2.0,3.1415,4.0,5.14");
        listaDoubleVazia = new ListaCSV<>(doubleFromString, stringifyDouble);
    }

    @Test
    public void acessar() {
        assertEquals((Double) 1.0, listaDouble.acessar(0));
        assertEquals((Double) 3.1415, listaDouble.acessar(2));
        assertEquals((Double) 5.14, listaDouble.acessar(4));
    }

    @Test
    public void inserir() {
        listaDouble.inserir(2, 4.0);
        listaDouble.inserir(8, 15.0);
        listaDouble.inserir(0, 18.0);
        assertEquals("18.0,1.0,2.0,4.0,3.1415,4.0,5.14,15.0", listaDouble.toString());
    }

    @Test
    public void remover() {
        assertEquals((Double) 1.0, listaDouble.remover(0));
        assertEquals((Double) 4.0, listaDouble.remover(2));
        assertEquals("2.0,3.1415,5.14", listaDouble.toString());
    }

    @Test(expected=RuntimeException.class)
    public void naoDeveRemover() {
        listaDouble.remover(8);
    }

    @Test
    public void pesquisar() {
        assertEquals(2, listaDouble.pesquisar(3.1415));
        assertEquals(0, listaDouble.pesquisar(1.0));
    }

    @Test(expected=RuntimeException.class)
    public void pesquisouElementoNaoExistente() {
        listaDouble.pesquisar(8.0);
    }

    @Test
    public void existe() {
        assertTrue(listaDouble.existe(3.1415));
        assertFalse(listaDouble.existe(8.0));
    }

    @Test
    public void qtdeElems() {
        assertEquals(5,listaDouble.qtdeElems());
        assertEquals(0, listaDoubleVazia.qtdeElems());
        listaDoubleVazia.inserir(0,1.0);
        assertEquals(1,listaDoubleVazia.qtdeElems());
    }

    @Test
    public void isVazia() {
        assertTrue(listaDoubleVazia.isVazia());
        assertFalse(listaDouble.isVazia());
    }

    @Test
    public void testToString() {
    }
}