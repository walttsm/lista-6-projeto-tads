package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PilhaIntegerCSVTest {
    Pilha<Integer> pilhaVazia;
    Pilha<Integer> pilhaPreenchida;
    @Before
    public void createPilha() {
         pilhaVazia = new PilhaIntegerCSV();
         pilhaPreenchida = new PilhaIntegerCSV("2,3,1,5");
    }

    @Test
    public void testToString() {
        assertEquals("", pilhaVazia.toString());
        assertEquals("2,3,1,5", pilhaPreenchida.toString());
    }

    @Test
    public void empilhar() {
        pilhaVazia.empilhar(2);
        assertEquals("2", pilhaVazia.toString());
        pilhaVazia.empilhar(3);
        assertEquals("2,3", pilhaVazia.toString());
        pilhaVazia.empilhar(1);
        assertEquals("2,3,1", pilhaVazia.toString());
        pilhaVazia.empilhar(5);
        assertEquals("2,3,1,5", pilhaVazia.toString());
        pilhaPreenchida.empilhar(6);
        assertEquals("2,3,1,5,6", pilhaPreenchida.toString());
    }

    @Test
    public void desempilhar() {
        assertEquals((Integer) 5, pilhaPreenchida.desempilhar());
        assertEquals((Integer) 1, pilhaPreenchida.desempilhar());
    }

    @Test
    public void olharTopo() {
        assertEquals((Integer) 5, pilhaPreenchida.olharTopo());
        pilhaPreenchida.desempilhar();
        assertEquals((Integer) 1, pilhaPreenchida.olharTopo());
        pilhaPreenchida.empilhar(6);
        assertEquals((Integer) 6, pilhaPreenchida.olharTopo());
    }

    @Test
    public void qtdeElems() {
        assertEquals(0, pilhaVazia.qtdeElems());
        pilhaVazia.empilhar(2);
        assertEquals(1, pilhaVazia.qtdeElems());
        assertEquals(4, pilhaPreenchida.qtdeElems());
        pilhaPreenchida.empilhar(6);
        assertEquals(5, pilhaPreenchida.qtdeElems());
    }

    @Test
    public void isVazia() {
        assertTrue(pilhaVazia.isVazia());
        pilhaVazia.empilhar(2);
        assertFalse(pilhaVazia.isVazia());
        assertFalse(pilhaPreenchida.isVazia());
    }
}