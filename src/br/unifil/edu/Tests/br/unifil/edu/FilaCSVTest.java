package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.*;

public class FilaCSVTest {
    private FilaCSV<Double> filaDouble;

    @Before
    public void setUp() throws Exception {
        Function<String, Double> doubleFromString = Double::valueOf;
        Function<Double, String> stringifyDouble = String::valueOf;
        filaDouble = new FilaCSV<>(doubleFromString, stringifyDouble);
        filaDouble.enfileirar(16.8);
        filaDouble.enfileirar(1.005);
    }

    @Test
    public void enfileirar() {
        filaDouble.enfileirar(3.0);
        assertEquals("3.0,1.005,16.8", filaDouble.toString());
        filaDouble.enfileirar(2.5);
        assertEquals("2.5,3.0,1.005,16.8", filaDouble.toString());
    }

    @Test
    public void desenfileirar() {
        filaDouble.enfileirar(3.0);
        filaDouble.enfileirar(2.5);
        assertEquals((Double) 16.8, filaDouble.desenfileirar());
        assertEquals((Double) 1.005, filaDouble.desenfileirar());
        assertEquals((Double) 3.0, filaDouble.desenfileirar());
    }

    @Test
    public void olharPrimeiro() {
        assertEquals((Double) 16.8, filaDouble.olharPrimeiro());
        filaDouble.desenfileirar();
        assertEquals((Double) 1.005, filaDouble.olharPrimeiro());
    }

    @Test
    public void qtdeElems() {
        assertEquals(2, filaDouble.qtdeElems());
        filaDouble.desenfileirar();
        assertEquals(1, filaDouble.qtdeElems());
        filaDouble.desenfileirar();
        assertEquals(0, filaDouble.qtdeElems());
    }

    @Test
    public void isVazia() {
        assertFalse(filaDouble.isVazia());
        filaDouble.desenfileirar();
        filaDouble.desenfileirar();
        assertTrue(filaDouble.isVazia());
    }
}