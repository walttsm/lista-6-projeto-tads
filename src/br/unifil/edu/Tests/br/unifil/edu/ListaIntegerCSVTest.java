package br.unifil.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ListaIntegerCSVTest {
    Lista<Integer> listaVazia;
    Lista<Integer> listaPreenchida;

    @Before
    public void setUp() {
        listaVazia = new ListaIntegerCSV();
        listaPreenchida = new ListaIntegerCSV("1,2,3,4,5");
    }

    @Test
    public void testToString() {
        assertEquals("",listaVazia.toString());
        assertEquals("1,2,3,4,5", listaPreenchida.toString());
    }

    @Test
    public void acessar() {
        assertEquals((Integer) 1, listaPreenchida.acessar(0));
        assertEquals((Integer) 3, listaPreenchida.acessar(2));
        assertEquals((Integer) 5, listaPreenchida.acessar(4));
    }

    @Test
    public void inserir() {
        listaVazia.inserir(5, 7);
        listaPreenchida.inserir(2, 4);
        listaPreenchida.inserir(8, 15);
        listaPreenchida.inserir(0, 18);
        assertEquals("7", listaVazia.toString());
        assertEquals("18,1,2,4,3,4,5,15", listaPreenchida.toString());
    }

    @Test
    public void remover() {
        listaVazia.inserir(0,7);
        assertEquals((Integer) 7, listaVazia.remover(0));
        assertEquals("", listaVazia.toString());
        assertEquals((Integer) 1, listaPreenchida.remover(0));
        assertEquals((Integer) 4, listaPreenchida.remover(2));
        assertEquals("2,3,5", listaPreenchida.toString());
    }

    @Test(expected=RuntimeException.class)
    public void naoDeveRemover() {
        listaPreenchida.remover(8);
    }


    @Test
    public void pesquisar() {
        assertEquals(2, listaPreenchida.pesquisar(3));
        assertEquals(0, listaPreenchida.pesquisar(1));
    }

    @Test(expected=RuntimeException.class)
    public void pesquisouElementoNaoExistente() {
        listaPreenchida.pesquisar(8);
    }

    @Test
    public void existe() {
        assertTrue(listaPreenchida.existe(3));
        assertFalse(listaPreenchida.existe(8));
    }

    @Test
    public void qtdeElems() {
        assertEquals(0, listaVazia.qtdeElems());
        listaVazia.inserir(0,1);
        assertEquals(1,listaVazia.qtdeElems());
        assertEquals(5,listaPreenchida.qtdeElems());
    }

    @Test
    public void isVazia() {
        assertTrue(listaVazia.isVazia());
        assertFalse(listaPreenchida.isVazia());
    }
}