package br.unifil.edu;

import java.util.function.Function;

public class ListaCSV<T> implements Lista<T> {
    Function<String, T> constructFromString;
    Function<T, String> stringify;
    String lista = "";

    public ListaCSV(Function<String, T> constructFromString, Function<T, String> stringify) {
        this.constructFromString = constructFromString;
        this.stringify = stringify;
    }

    public ListaCSV(Function<String, T> constructFromString, Function<T, String> stringify, String lista) {
            this.constructFromString = constructFromString;
            this.stringify = stringify;
            this.lista = lista;
    }


    /**
     * Theta(1), mas mas devido ao split() -> Omega(1) O(n)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice do elemento a ser acessado.
     * @return O numero acessado como Integer.
     */
    @Override
    public T acessar(int indice) {
        return constructFromString.apply(this.lista.split(",")[indice]);
    }

    /**
     * Omega(1), O(n) -> mas devido ao split(), concat() e substring() -> Omega(n²) e O(n³)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice onde o elemento será adicionado.
     * @param obj Elemento a ser adicionado.
     */
    @Override
    public void inserir(int indice, T obj) {
        String novalista = "";
        if (indice == 0) {
            novalista = obj.toString() + ",";
            this.lista = novalista.concat(this.lista);
        } else if (indice >= this.lista.split(",").length - 1) {
            this.lista = ((indice > this.lista.split(",").length - 1) && !this.lista.equals("")) ?
                    this.lista.concat("," + obj.toString()) :
                    this.lista.concat(obj.toString());
        } else {
            int cutIdx = 0; int virgulas = 0;
            for (int idxString = 0; idxString < lista.length(); idxString++) {
                if (lista.charAt(idxString) == ',') {
                    virgulas++;
                }
                if (virgulas == indice) {
                    cutIdx = idxString + 1;
                    break;
                }
            }
            novalista = novalista.concat(this.lista.substring(0, cutIdx)) + obj.toString() + ","; // concat e substring -> n²
            this.lista = novalista.concat(this.lista.substring(cutIdx));
        }
    }

    /**
     * Omega(1), O(n) -> mas devido ao split(), concat() e substring() -> Omega(n) e O(n³)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice onde de onde o elemento será removido.
     * @return
     */
    @Override
    public T remover(int indice) {
        String novalista = "";
        String removido;
        if (indice >= this.lista.split(",").length || indice < 0)
            throw new RuntimeException("Quer pegar índice fora da lista? Tá doido...");
        else if (indice == 0) {
            removido= this.lista.substring(0, this.lista.indexOf(","));
            this.lista = this.lista.substring(this.lista.indexOf(",") + 1);
        } else if (indice == this.lista.split(",").length - 1) {
            removido = this.lista.substring(this.lista.lastIndexOf(","));
        } else {
            int cutIdx = 0; int virgulas = 0;
            for (int idxString = 0; idxString < lista.length(); idxString++) {
                if (lista.charAt(idxString) == ',') {
                    virgulas++;
                }
                if (virgulas == indice) {
                    cutIdx = idxString + 1;
                    break;
                }
            }
            novalista = novalista.concat(this.lista.substring(0, cutIdx));
            String listaDireita = this.lista.substring(cutIdx);
            removido = listaDireita.substring(0, listaDireita.indexOf(","));
            this.lista = novalista.concat(listaDireita.substring(listaDireita.indexOf(",") + 1));
        }
        return constructFromString.apply(removido);
    }

    /**
     * Omega(1), O(n), mas devido ao split() -> Omega(n) e O(n²)
     * Encadeadas:
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento a ser buscado.
     * @return
     */
    @Override
    public int pesquisar(T obj) {
        String[] lista = this.lista.split(",");
        for (int i = 0; i < lista.length; i++) {
            if (lista[i].equals(stringify.apply(obj))) return i;
        }
        throw new RuntimeException("Elemento não encontrado na lista!");
    }

    /**
     * Omega(1), O(n), mas devido ao split() -> Omega(n) e O(n²)
     * Encadeadas:
     * Arranjadas:
     * @param obj Elemento de checagem.
     * @return
     */
    @Override
    public boolean existe(T obj) {
        String[] lista = this.lista.split(",");
        for (String str : lista) {
            if (constructFromString.apply(str).equals(obj)) return true;
        }
        return false;
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) O(n)
     * Encadeadas:
     * Arranjadas:
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.lista.equals("") ? 0 : this.lista.split(",").length;
    }

    /**
     * Theta(1)
     * Encadeadas:
     * Arranjadas:
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.lista.equals("");
    }

    /**
     * Theta(1)
     * @return A lista como string.
     */
    @Override
    public String toString() {
        return this.lista;
    }
}
