package br.unifil.edu;


public interface Colecao<T> {
    /**
     * Busca o número de elementos na lista.
     * @return O número de elementos da lista.
     */
    int qtdeElems();

    /**
     * Verifica se a lista está vazia.
     * @return TRUE se a coleção estiver vazia, senão retorna FALSE
     */
    boolean isVazia();

}
