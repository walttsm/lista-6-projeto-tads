package br.unifil.edu;

/*
###CONCLUSÃO DA ANÁLISE ASSINTÓTICA
    Se compararmos os comportamentos dos métodos, vemos que o comportamento se assemelha as TADs arranjadas,
    que oferecem melhor desempenho na maioria dos casos, no entanto, podem ser piores em casos específicos.
    Neste caso, os métodos de String usados para implementação teriam grande influência no desempenho dos métodos,
    por isso, foram levadas em consideração suas complexidades.
 */

public class FilaIntegerCSV implements Fila<Integer>{
    String fila ="";
    public FilaIntegerCSV() {}

    public FilaIntegerCSV(String fila) {
        this.fila = fila;
    }

    /**
     * Omega(1), O(n), mas devido ao concat() -> Omega(n), O(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento adicionado à fila.
     */
    @Override
    public void enfileirar(Integer obj) {
        String novaFila = this.fila.equals("") ?  obj.toString() : obj.toString() + ","; // 1
        this.fila = novaFila.concat(this.fila); // n
    }

    /**
     * Theta(1), mas devido ao substring() e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public Integer desenfileirar() {
        String obj= this.fila.substring(this.fila.lastIndexOf(",") + 1);
        this.fila = this.fila.substring(0, this.fila.lastIndexOf(","));
        return Integer.parseInt(obj);
    }

    /**
     * Theta(1), mas devido ao substring e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public Integer olharPrimeiro() {
        String obj= this.fila.substring(this.fila.lastIndexOf(",") + 1);
        return Integer.parseInt(obj);
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) O(n)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.fila.equals("") ? 0 : this.fila.split(",").length; //o equals pode verificar o tamanho
                                                                              // da string, portanto considerar Theta(1)
    }

    /**
     * Theta(1)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.fila.equals("");
    }

    /**
     * Theta(1)
     * @return A fila como string.
     */
    @Override
    public String toString() {
        return this.fila;
    }
}
