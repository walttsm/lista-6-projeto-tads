package br.unifil.edu;

import java.util.function.Function;

public class FilaCSV<T> implements Fila<T> {
    Function<String, T> constructFromString;
    Function<T, String> stringify;
    String fila = "";

    public FilaCSV(Function<String, T> constructFromString, Function<T, String> stringify) {
        this.constructFromString = constructFromString;
        this.stringify = stringify;
    }

    public FilaCSV(Function<String, T> constructFromString, Function<T, String> stringify, String fila) {
        this.constructFromString = constructFromString;
        this.stringify = stringify;
        this.fila = fila;
    }

    /**
     * Omega(1), O(n), mas devido ao concat() -> Omega(n), O(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento adicionado à fila.
     */
    @Override
    public void enfileirar(T obj) {
        String novaFila = this.fila.equals("") ?  stringify.apply(obj) : stringify.apply(obj) + ","; // 1
        this.fila = novaFila.concat(this.fila); // n
    }

    /**
     * Theta(1), mas devido ao substring() e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public T desenfileirar() {
        String obj;
        if (this.fila.contains(",")) {
            obj = this.fila.substring(this.fila.lastIndexOf(",") + 1);
            this.fila = this.fila.substring(0, this.fila.lastIndexOf(","));
        } else {
            obj = this.fila;
            this.fila = "";
        }
        return constructFromString.apply(obj);
    }

    /**
     * Theta(1), mas devido ao substring e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public T olharPrimeiro() {
        String obj= this.fila.substring(this.fila.lastIndexOf(",") + 1);
        return constructFromString.apply(obj);
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) O(n)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.fila.equals("") ? 0 : this.fila.split(",").length; //o equals pode verificar o tamanho
        // da string, portanto considerar Theta(1)
    }

    /**
     * Theta(1)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.fila.equals("");
    }

    /**
     * Theta(1)
     * @return A fila como string.
     */
    @Override
    public String toString() {
        return this.fila;
    }
}


