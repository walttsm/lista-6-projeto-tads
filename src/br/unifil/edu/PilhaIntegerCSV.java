package br.unifil.edu;

/*
###CONCLUSÃO DA ANÁLISE ASSINTÓTICA
    Se compararmos os comportamentos dos métodos, vemos que o comportamento se assemelha as TADs arranjadas,
    que oferecem melhor desempenho na maioria dos casos, no entanto, podem ser piores em casos específicos.
    Neste caso, os métodos de String usados para implementação teriam grande influência no desempenho dos métodos,
    por isso, foram levadas em consideração suas complexidades.
 */

public class PilhaIntegerCSV implements Pilha<Integer>{
    String pilha ="";

    public PilhaIntegerCSV() {}

    public PilhaIntegerCSV(String pilha) {
        this.pilha = pilha;
    }

    /**
     * Omega(1), O(n), mas devido ao string.concat -> Omega(n) O(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento a ser colocado na pilha.
     */
    @Override
    public void empilhar(Integer obj) {
        if (this.pilha.length() == 0) // 1
            this.pilha = this.pilha.concat(obj.toString()); // n
        else // 1
            this.pilha = this.pilha.concat("," + obj.toString()); // n
    }

    /**
     * Theta(1), mas devido ao substring() e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public Integer desempilhar() {
        String obj = this.pilha.substring(this.pilha.lastIndexOf(",") + 1); // n * n
        this.pilha = this.pilha.substring(0, this.pilha.lastIndexOf(",")); // n * n
        return Integer.parseInt(obj); // 1
    }

    /**
     * Theta(1), mas devido ao substring e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public Integer olharTopo() {
        String obj= this.pilha.substring(this.pilha.lastIndexOf(",") + 1); // n* n
        return Integer.parseInt(obj); // 1
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) Theta(n)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.pilha.equals("") ? 0 : this.pilha.split(",").length; // 1, o equals pode verificar o tamanho
                                                                                // da string, portanto considerar Theta(1)
    }

    /**
     * Theta(1)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.pilha.equals(""); // 1
    }

    /**
     * Theta(1)
     * @return A pilha como string.
     */
    @Override
    public String toString() {
        return this.pilha; // 1
    }

}
