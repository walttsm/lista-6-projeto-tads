package br.unifil.edu;

import java.util.function.Function;

public class PilhaCSV<T> implements Pilha<T> {
    Function<String, T> constructFromString;
    Function<T, String> stringify;
    String pilha ="";

    public PilhaCSV(Function<String, T> constructFromString, Function<T, String> stringify) {
        this.constructFromString = constructFromString;
        this.stringify = stringify;
    }

    public PilhaCSV(Function<String, T> constructFromString, Function<T, String> stringify, String pilha) {
        this.constructFromString = constructFromString;
        this.stringify = stringify;
        this.pilha = pilha;
    }

    /**
     * Omega(1), O(n), mas devido ao string.concat -> Omega(n) O(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento a ser colocado na pilha.
     */
    @Override
    public void empilhar(T obj) {
        if (this.pilha.length() == 0) // 1
            this.pilha = this.pilha.concat(stringify.apply(obj)); // n
        else // 1
            this.pilha = this.pilha.concat("," + stringify.apply(obj)); // n
    }

    /**
     * Theta(1), mas devido ao substring() e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public T desempilhar() {
        String obj;
        if (this.pilha.contains(",")) {
            obj = this.pilha.substring(this.pilha.lastIndexOf(",") + 1); // n * n
            this.pilha = this.pilha.substring(0, this.pilha.lastIndexOf(",")); // n * n
        } else {
            obj = this.pilha;
            this.pilha = "";
        }
        return constructFromString.apply(obj); // 1
    }

    /**
     * Theta(1), mas devido ao substring e o lastIndexOf() -> Theta(n²)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public T olharTopo() {
        String obj= this.pilha.substring(this.pilha.lastIndexOf(",") + 1); // n* n
        return constructFromString.apply(obj); // 1
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) Theta(n)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.pilha.equals("") ? 0 : this.pilha.split(",").length; // 1, o equals pode verificar o tamanho
        // da string, portanto considerar Theta(1)
    }

    /**
     * Theta(1)
     * Encadeadas: Theta(1)
     * Arranjadas: Theta(1)
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.pilha.equals(""); // 1
    }

    /**
     * Theta(1)
     * @return A pilha como string.
     */
    @Override
    public String toString() {
        return this.pilha; // 1
    }

}
