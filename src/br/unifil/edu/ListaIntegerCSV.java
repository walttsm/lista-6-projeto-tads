package br.unifil.edu;

/*
###CONCLUSÃO DA ANÁLISE ASSINTÓTICA
    Se compararmos os comportamentos dos métodos, vemos que o comportamento se assemelha as TADs arranjadas,
    que oferecem melhor desempenho na maioria dos casos, no entanto, podem ser piores em casos específicos.
    Neste caso, os métodos de String usados para implementação teriam grande influência no desempenho dos métodos,
    por isso, foram levadas em consideração suas complexidades.
 */

public class ListaIntegerCSV implements Lista<Integer> {
    String lista = "";

    public ListaIntegerCSV(){}

    public ListaIntegerCSV(String lista) {
        this.lista = lista;
    }


    /**
     * Theta(1), mas mas devido ao split() -> Omega(1) O(n)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice do elemento a ser acessado.
     * @return O numero acessado como Integer.
     */
    @Override
    public Integer acessar(int indice) {
        return Integer.parseInt(this.lista.split(",")[indice]);
    }

    /**
     * Omega(1), O(n) -> mas devido ao split(), concat() e substring() -> Omega(n²) e O(n³)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice onde o elemento será adicionado.
     * @param obj Elemento a ser adicionado.
     */
    @Override
    public void inserir(int indice, Integer obj) {
        String novalista = "";
        if (indice == 0) {
            novalista = obj.toString() + ",";
            this.lista = novalista.concat(this.lista);
        } else if (indice >= this.lista.split(",").length - 1) {
            this.lista = ((indice > this.lista.split(",").length - 1) && !this.lista.equals("")) ?
                    this.lista.concat("," + obj.toString()) :
                    this.lista.concat(obj.toString());
        } else {
            int cutIdx = 0; int virgulas = 0;
            for (int idxString = 0; idxString < lista.length(); idxString++) {
                if (lista.charAt(idxString) == ',') {
                    virgulas++;
                }
                if (virgulas == indice) {
                    cutIdx = idxString + 1;
                    break;
                }
            }
            novalista = novalista.concat(this.lista.substring(0, cutIdx)) + obj.toString() + ","; // concat e substring -> n²
            this.lista = novalista.concat(this.lista.substring(cutIdx));
        }
    }

    /**
     * Omega(1), O(n) -> mas devido ao split(), concat() e substring() -> Omega(n) e O(n³)
     * Encadeadas:
     * Arranjadas:
     * @param indice Índice onde de onde o elemento será removido.
     * @return
     */
    @Override
    public Integer remover(int indice) {
        String novalista = "";
        String removido;
        if (indice >= this.lista.split(",").length || indice < 0)
            throw new RuntimeException("Quer pegar índice fora da lista? Tá doido...");
        else if (indice == 0) {
            removido= this.lista.substring(0, this.lista.indexOf(","));
            this.lista = this.lista.substring(this.lista.indexOf(",") + 1);
        } else if (indice == this.lista.split(",").length - 1) {
            removido = this.lista.substring(this.lista.lastIndexOf(","));
        } else {
            int cutIdx = 0; int virgulas = 0;
            for (int idxString = 0; idxString < lista.length(); idxString++) {
                if (lista.charAt(idxString) == ',') {
                    virgulas++;
                }
                if (virgulas == indice) {
                    cutIdx = idxString + 1;
                    break;
                }
            }
            novalista = novalista.concat(this.lista.substring(0, cutIdx));
            String listaDireita = this.lista.substring(cutIdx);
            removido = listaDireita.substring(0, listaDireita.indexOf(","));
            this.lista = novalista.concat(listaDireita.substring(listaDireita.indexOf(",") + 1));
        }
        return Integer.parseInt(removido);
    }

    /**
     * Omega(1), O(n), mas devido ao split() -> Omega(n) e O(n²)
     * Encadeadas:
     * Arranjadas: O(n), Omega(1)
     * @param obj Elemento a ser buscado.
     * @return
     */
    @Override
    public int pesquisar(Integer obj) {
        String[] lista = this.lista.split(",");
        for (int i = 0; i < lista.length; i++) {
            if (lista[i].equals(obj.toString())) return i;
        }
        throw new RuntimeException("Elemento não encontrado na lista!");
    }

    /**
     * Omega(1), O(n), mas devido ao split() -> Omega(n) e O(n²)
     * Encadeadas:
     * Arranjadas:
     * @param obj Elemento de checagem.
     * @return
     */
    @Override
    public boolean existe(Integer obj) {
        String[] lista = this.lista.split(",");
        for (String str : lista) {
            if (Integer.parseInt(str) == obj) return true;
        }
        return false;
    }

    /**
     * Theta(1), mas devido ao split() -> Omega(1) O(n)
     * Encadeadas:
     * Arranjadas:
     * @return
     */
    @Override
    public int qtdeElems() {
        return this.lista.equals("") ? 0 : this.lista.split(",").length;
    }

    /**
     * Theta(1)
     * Encadeadas:
     * Arranjadas:
     * @return
     */
    @Override
    public boolean isVazia() {
        return this.lista.equals("");
    }

    /**
     * Theta(1)
     * @return A lista como string.
     */
    @Override
    public String toString() {
        return this.lista;
    }
}
